import {MigrationInterface, QueryRunner} from "typeorm";

export class recipes1512733971734 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(
            `INSERT INTO recipe VALUES
            (1, 'This is the first description', 1), 
            (2, 'second description', 2), 
            (3, 'third description', 3), 
            (5, 'daily sounds', 4), 
            (6, 'ahmad tea', 1), 
            (7, '25 years ago', 1), 
            (8, 'Eduard phone', 1), 
            (9, '29 characters', 4), 
            (10, 'This one is the last', 2);`
        );
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
