import {MigrationInterface, QueryRunner} from "typeorm";

export class users1512733964812 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query(
            `INSERT INTO "user" VALUES
            (1, 'Jhon', 'Smith', true), 
            (2, 'Hovhannes', 'Tumanyan', true),
            (3, 'Tigran', 'Mkhitaryan', false), 
            (4, 'Vardan', 'Badalyan', true);`
        );
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
    }

}
