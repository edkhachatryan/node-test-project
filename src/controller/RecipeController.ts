import {getRepository} from "typeorm";
import {NextFunction, Request, Response} from "express";
import {Recipe} from "../entity/Recipe";

export class RecipeController {

    private recipeRepository = getRepository(Recipe);

    async find(request: Request, response: Response, next: NextFunction) {
        const limit = parseInt(request.query.limit) || 10;
        const offset = parseInt(request.query.offset) || 0;
        return this.recipeRepository
            .createQueryBuilder("recipe")
            .leftJoinAndSelect("recipe.user", "user")
            .skip(offset)
            .take(limit)
            .getMany();
    }

    async findOne(request: Request, response: Response, next: NextFunction) {
        return this.recipeRepository.findOneById(request.params.id, { relations: ["user"] });
    }

    async create(request: Request, response: Response, next: NextFunction) {
        const data = request.body || {};

        // TODO: It's needed create also relation
        return this.recipeRepository
            .createQueryBuilder()
            .insert()
            .into(Recipe)
            .values([{
                description: data.description
            }])
            .execute();
    }

    async update(request: Request, response: Response, next: NextFunction) {
        const data = request.body || {};
        return this.recipeRepository
            .createQueryBuilder()
            .update(Recipe)
            .set({
                description: data.description,
            })
            .where("id = :id", { id: data.id })
            .execute();
    }
}