import {getRepository} from "typeorm";
import {NextFunction, Request, Response} from "express";
import {User} from "../entity/User";

export class UserController {

    private userRepository = getRepository(User);

    async find(request: Request, response: Response, next: NextFunction) {
        const limit = parseInt(request.query.limit) || 10;
        const offset = parseInt(request.query.offset) || 0;

        let q = this.userRepository
            .createQueryBuilder("user")
            .leftJoinAndSelect("user.recipes", "recipe")
            .skip(offset)
            .take(limit);

        if (request.query.name) {
            return q.where("user.firstName = :firstName", { firstName: request.query.name })
                .getMany()
        }

        return q.getMany();

    }

    async findOne(request: Request, response: Response, next: NextFunction) {
        return this.userRepository.findOneById(request.params.id, { relations: ["recipes"] });
    }

    async create(request: Request, response: Response, next: NextFunction) {
        return this.userRepository
            .createQueryBuilder()
            .insert()
            .into(User)
            .values([request.body])
            .execute();
    }

    async update(request: Request, response: Response, next: NextFunction) {
        const data = request.body || {};
        return this.userRepository
            .createQueryBuilder()
            .update(User)
            .set({
                firstName: data.firstName,
                lastName: data.lastName,
                isAdmin: data.isAdmin
            })
            .where("id = :id", { id: data.id })
            .execute();
    }

}