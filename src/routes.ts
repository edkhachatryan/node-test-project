import {UserController} from "./controller/UserController";
import {RecipeController} from "./controller/RecipeController";

export const Routes = [{
    method: "get",
    route: "/users",
    controller: UserController,
    action: "find"
}, {
    method: "get",
    route: "/users/:id",
    controller: UserController,
    action: "findOne"
}, {
    method: "post",
    route: "/users",
    controller: UserController,
    action: "create"
}, {
    method: "put",
    route: "/users",
    controller: UserController,
    action: "update",
    admin: true
},{
    method: "get",
    route: "/recipes",
    controller: RecipeController,
    action: "find"
}, {
    method: "get",
    route: "/recipes/:id",
    controller: RecipeController,
    action: "findOne"
}, {
    method: "post",
    route: "/recipes",
    controller: RecipeController,
    action: "create"
}, {
    method: "put",
    route: "/recipes",
    controller: RecipeController,
    action: "update"
}];