import * as server from "../index";
import "reflect-metadata";
import * as chai from "chai";
import * as chaiHttp from "chai-http";
import "mocha";
import * as rp from "request-promise";

chai.use(chaiHttp);

declare let describe;
declare let it;
declare let before;

describe("UserController", function() {
    this.timeout(10000);
    before((done) => {
        setTimeout(() => {
            done();
        }, 5000);
    });
    describe("#find()", function() {

        it("should get all users", (done) => {
            chai.request(server)
                .get('/users')
                .end((err, res) => {
                    if (err) {
                       return;
                    }
                    done();
                });

        });
    });

});