import {Entity, PrimaryGeneratedColumn, Column, ManyToOne} from "typeorm";
import {User} from './User'

@Entity()
export class Recipe {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    description: string;

    @ManyToOne(type => User, user => user.recipes)
    user: User;
}