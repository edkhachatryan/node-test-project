#  Node test project with TypeORM

## Installation

It should be installed postgreSQL on your machine
and set database configurations in `ormconfig.json` file.

## Cloning

```
1. git clone https://bitbucket.org/edkhachatryan/node-test-project.git
2. cd node-test-project
3. npm i
```

## Run project

To run the project use the following command:
 ```
 npm start
 ```

## Migration
To insert dummy data to the database run this command:
```
npm run migrations
```

## Tests
To run unit tests do the following:
```
npm test
```

## Documentation

1. find all users `GET /users`
    optional query parameters: `offset, limit, name`
    ```
    /users?limit=2&offset=22&name=Tiko
    ```

2. find one user `GET /users/:userId`

3. create new user `POST /users`
    body should be like this:
    ```
    {
        "firstName": "Tiko",
        "lastName": "Mkhitaryan",
        "isAdmin": false
    }
    ```

4. update user `PUT /users`
    only admin can request to this endpoint. Request headers
    should contains the following key:
    ```
    ...
    admin: true
    ...
    ```
    body should be like this:
    ```
    {
        "id": 6,
        "firstName": "Tiko",
        "lastName": "Mkhitaryan",
        "isAdmin": false
    }
    ```

5. find all recipes `GET /recipes`
    parameters: `offset, limit`

6. find one user `GET /recipes/:recipeId`

7. create new recipe `POST /recipes`
    body should be like this:
    ```
    {
        "description": "Tiko"
    }
    ```

8. update user `PUT /users`
    body should be like this:
    ```
    {
        "id": 6,
        "description": "Tiko"
    }
    ```